ARG IMAGE_VERSION=latest
FROM seafileltd/seafile:${IMAGE_VERSION}

COPY templates/seafile.nginx.conf.template /templates/seafile.nginx.conf.template
