# Seafile Docker Image

The Dockerfile requires an build argument for specifying which base image to use:

```
$ docker build --build-arg IMAGE_VERSION=6.3.2 -t lawitschka/seafile:6.3.2 .
```
